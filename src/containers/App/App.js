import React, {useState} from 'react';
import {nanoid} from "nanoid";
import Order from '../../components/Order/Order'
import './App.css';
import Ingredients from "../../components/Ingredients/Ingredients";

const INGREDIENTS = [
    {name: 'Meat', price: 50},
    {name: 'Cheese', price: 20},
    {name: 'Salad', price: 5},
    {name: 'Bacon', price: 30},
]
const App = () => {
    const [ingredients, setIngredients] = useState([
        {name: 'Meat', count: 0, id: nanoid()},
        {name: 'Cheese', count: 0, id: nanoid()},
        {name: 'Salad', count: 0, id: nanoid()},
        {name: 'Bacon', count: 0, id: nanoid()},
    ]);

    const countIngredient = id => {
        const index = ingredients.findIndex(i => i.id === id);
        const ingredientsCopy = [...ingredients];
        const ingredientCopy = ingredientsCopy[index];
        ingredientCopy.count++;

        setIngredients(ingredientsCopy);
    };

    const totalSpent = () => {
        let total = 0;

        for (let i = 0; i < INGREDIENTS.length; i++) {
            const nameObj = INGREDIENTS[i];
            const name = nameObj.name;
            const price = nameObj.price;

            const count = ingredients.filter(el => el.name === name)[0].count;

            total += price * count;
        }

        return total;
    }

    const removeIngredient = id => {
        const index = ingredients.findIndex(i => i.id === id);
        const ingredientsCopy = [...ingredients];
        const ingredientCopy = ingredientsCopy[index];
        ingredientCopy.count--;

        setIngredients(ingredientsCopy);
    };

    const getIngs = (ingName) => {
        const arr = [];
        const ing = ingredients.filter(ing => ing.name === ingName)[0];
        const count = ing.count ? ing.count : 1;

        for (let i = 0; i < count; i++){
            arr.push(<Order key={ingName + arr.length} className={ing.name}/>)
        }

        return arr;
    }

    return (
        <div className="container">
            <div className="ingredients-item">
                <h2 className="ingredients-item__name">Ingredients</h2>
                <Ingredients
                    ingredients={ingredients}
                    countIngredient={countIngredient}
                    removeIngredient={removeIngredient}
                />
            </div>
            <div className="burger-item">
                <h2 className="burger-item__name">Burger</h2>
                <div className="Burger">
                    <div className="BreadTop">
                        <div className="Seeds1"></div>
                        <div className="Seeds2"></div>
                    </div>
                    {ingredients.map((ingr, index) => {
                        if(ingr.count > 0) return getIngs(ingr.name)
                    })}
                    <div className="BreadBottom"></div>
                </div>
                <p>Total: {totalSpent()} KGS</p>
            </div>
        </div>
    )
}

export default App;
