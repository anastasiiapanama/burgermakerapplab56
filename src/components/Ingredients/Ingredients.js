import React from 'react';
import Ingredient from "../Ingredient/Ingredient";

const Ingredients = props => {
    return props.ingredients.map(ingredient => (
       <Ingredient
           key={ingredient.id}
           countIngredients={() => props.countIngredient(ingredient.id)}
           name={ingredient.name}
           count={ingredient.count}
           removeIngredient={() => props.removeIngredient(ingredient.id)}
       />
    ));
};

export default Ingredients;