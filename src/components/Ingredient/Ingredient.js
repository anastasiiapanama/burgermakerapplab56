import React from 'react';

const Ingredient = props => {
    return (
        <div className="ingredients-list">
            <button onClick={props.countIngredients}>Add</button>
            <h5>{props.name}</h5>
            <p>x {props.count}</p>
            <button onClick={props.removeIngredient}>Delete</button>
        </div>
    );
};

export default Ingredient;